package b137.delacruz.s02a1;

import java.util.Scanner;

public class LeapYearCalculator {

    public static void main(String[] args){
        System.out.println("Leap year calculator");

        //Scanner appScanner = new Scanner(System.in);

        //System.out.println("What is your first name? \n");
        //String firstName = appScanner.nextLine();

        //System.out.println("Hello, " + firstName + "!\n");

        System.out.println("Enter the year: \n");

        Scanner yearScanner = new Scanner(System.in);
        int year = yearScanner.nextInt();

        if(year%4 == 0){
            if(year%100 != 0){
                System.out.println(year + " is a leap year");
            } else if(year%400 == 0){
                System.out.println(year + " is a leap year");
            } else{
                System.out.println(year + " is not a leap year");
            }
        } else{
          System.out.println(year + " is not a leap year");
        };
    }
}
